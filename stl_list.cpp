// Lab - Standard Template Library - Part 2 - Lists
// FIRSTNAME, LASTNAME

#include <iostream>
#include <list>
#include <string>
using namespace std;
void DisplayList(list<string>& states)
{
	for (
		/*initiaization code*/
		list<string>::iterator it = states.begin();
		/*keep looping while*/
		it != states.end();
		/*run after each iteration*/
		it++

		) 
	{
		// display this element of the list
		cout << *it << "\t";
	}

}


int main()
{

	list<string> states;

		bool done = false;
		while (!done)
		{
			cout << "1. Push front "
				<< "2. Push back "
				<< "3. Pop front "
				<< "4. Pop back "
				<< "5. Continue"
				<< endl;


			int choice;
			cin >> choice;

			switch (choice)
			{
				{
			case 1:			// push front
				cout << "Enter a new state: ";
				string state;
				cin >> state;
				states.push_front(state);
				break;
				}
				{
			case 2://push back
				cout << "Enter a new state: ";
				string state;
				cin >> state;
				states.push_back(state);

				break;
				}
			case 3:
				states.pop_front();
				break;
			case 4:
				states.pop_back();

				break;
			case 5:
				done = true;
				break;

			}


		}
		cout << "list of states" << endl;
		cout << "original: ";
		DisplayList(states);
		
		cout << "Sorted: ";
		states.sort();
		DisplayList(states);

		cout << "Revers sort: ";
		states.reverse();
		DisplayList(states);


    cin.ignore();
    cin.get();
    return 0;
}
