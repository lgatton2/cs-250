// Lab - Standard Template Library - Part 3 - Queues
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> pending;

	bool done = false;
	while (!done)
	{
		string choice;
		float x =0;
		cout << "Enter new trasaction or continue: " << endl << endl;
		cin >> choice;

		if (choice == "new")
		{
			cin >> x;
			pending.push(x);
		}
		else if (choice=="continue")
		{
			
			done = true;
		}

	}
	float x = 0;
	while (!pending.empty())
	{
		
		cout << pending.front() << endl;
		x= x + pending.front();
		pending.pop();
	}
	cout << "Final balance: " << x;


    cin.ignore();
    cin.get();
    return 0;
}
/*
Enter new trasaction or continue:

new
5
Enter new trasaction or continue:

new
10
Enter new trasaction or continue:

continue
5
10
Final balance: 15

*/