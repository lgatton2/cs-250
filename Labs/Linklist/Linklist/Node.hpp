#ifndef _NODE_HPP
#define _NODE_HPP

template <typename T>
struct Node
{
	Node<T>* ptrNext;
	Node<T>* ptrPrev;
	T data;


};




#endif // !_NODE_HPP
