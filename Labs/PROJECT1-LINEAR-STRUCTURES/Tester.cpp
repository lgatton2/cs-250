#include "Tester.hpp"
#include <string>
void Tester::RunTests()
{
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_Remove();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;

    // Put tests here
}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

    // Put tests here
	{// test begin
		List<int> testlist;
		bool expectedOut = false;
		bool actualOut = testlist.ShiftRight(1);

		cout << "Expected out: " << expectedOut << endl;
		cout << "Actual out:   " << actualOut << endl;

		if (actualOut == expectedOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
		{//test 2

		List<int> testlist;
		testlist.PushBack(2);
		bool expectedOut = false;
		bool actualOut = testlist.ShiftRight(1);

		

		cout << "Expected out: " << expectedOut << endl;
		cout << "Actual out:   " << actualOut << endl;

		if (actualOut == expectedOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}
		//test 3
		{
		List<int> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack(i);
		}
		bool expectedOut = true;
		bool actualOut = testlist.ShiftRight(1);

		

		cout << "Expected out: " << expectedOut << endl;
		cout << "Actual out:   " << actualOut << endl;

		if (actualOut == expectedOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}
}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

	{// test begin
		List<int> testlist;
		bool expectedOut = false;
		bool actualOut = testlist.ShiftLeft(1);

		cout << "Expected out: " << expectedOut << endl;
		cout << "Actual out:   " << actualOut << endl;

		if (actualOut == expectedOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{//test 2

		List<int> testlist;
		testlist.PushBack(2);
		bool expectedOut = false;
		bool actualOut = testlist.ShiftLeft(1);

		

		cout << "Expected out: " << expectedOut << endl;
		cout << "Actual out:   " << actualOut << endl;

		if (actualOut == expectedOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}
	//test 3
	{
		List<int> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack(i);
		}
		bool expectedOut = true;
		bool actualOut = testlist.ShiftLeft(2);

		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack(i);
		}

		cout << "Expected out: " << expectedOut << endl;
		cout << "Actual out:   " << actualOut << endl;

		if (actualOut == expectedOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}
}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test begin
        cout << endl << "Test 1" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end

    {   // Test begin
        cout << endl << "Test 2" << endl;
        List<int> testList;

        testList.PushBack( 1 );

        int expectedSize = 1;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end
}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;

    // Put tests here 
	{
		// Test 1
		cout << endl << "Test 1" << endl;
		List<int> listA;
		bool expectedValue = true;
		bool actualValue = listA.IsEmpty();
		cout << "Expected value:" << expectedValue << endl;
		cout << "Actual value:" << actualValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Test Pass" << endl;
		}
		else
		{
			cout << "Test Fail" << endl;
		}
	}
	{
		// test 2
		cout << endl << "Test 2" << endl;
		List<int> listA;
		listA.PushBack(5);
		bool expectedValue = false;
		bool actualValue = listA.IsEmpty();
		cout << "Expected value:" << expectedValue << endl;
		cout << "Actual value:" << actualValue << endl;
		if (expectedValue == actualValue)
		{
			cout << "Test Pass" << endl;
		}
		else
		{
			cout << "Test False" << endl;
		}
	}
}

void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

    // test begin
	{
		{// test begin
			List<int> testlist;
			bool expectedOut = false;
			bool actualOut = testlist.IsFull();

			cout << "Expected out: " << expectedOut << endl;
			cout << "Actual out:   " << actualOut << endl;

			if (actualOut == expectedOut)
			{
				cout << "Pass" << endl;
			}
			else
			{
				cout << "Fail" << endl;
			}
		}
		{//test 2

			List<int> testlist;
			testlist.PushBack(2);
			bool expectedOut = false;
			bool actualOut = testlist.IsFull();

			

			cout << "Expected out: " << expectedOut << endl;
			cout << "Actual out:   " << actualOut << endl;

			if (actualOut == expectedOut)
			{
				cout << "Pass" << endl;
			}
			else
			{
				cout << "Fail" << endl;
			}

		}
		//test 3
		{
			List<int> testlist;
				for(int i = 0; i < 100; i++)
				{
					testlist.PushBack(i);
				}
			bool expectedOut = true;
			bool actualOut = testlist.IsFull();

			

			cout << "Expected out: " << expectedOut << endl;
			cout << "Actual out:   " << actualOut << endl;

			if (actualOut == expectedOut)
			{
				cout << "Pass" << endl;
			}
			else
			{
				cout << "Fail" << endl;
			}

		}
		{ //test 4
			{
				List<int> testlist;
				for (int i = 0; i < 101; i++)
				{
					testlist.PushBack(i);
				}
				bool expectedOut = true;
				bool actualOut = testlist.IsFull();

				

				cout << "Expected out: " << expectedOut << endl;
				cout << "Actual out:   " << actualOut << endl;

				if (actualOut == expectedOut)
				{
					cout << "Pass" << endl;
				}
				else
				{
					cout << "Fail" << endl;
				}

			}


		}


	}
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

    // test begin
	{
		cout << endl << "Test 1, creat a list and inset one item" << endl;

		List<string> testlist;
		bool expectedValue = true;
		bool actualValue = testlist.PushFront("A");

		cout << "Expected value:" << expectedValue << endl;
		cout << "Actual values:" << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test Pass" << endl;
		}
		else
		{
			cout << "Test False" << endl;
		}
	}

	{
		//test 2
		cout << "Test 2, insert 101 items" << endl;
		List<string> testlist;
		for (int i = 0; i < 100; i++)
			testlist.PushBack("Z");

		bool expectedValue = false;
		bool actualValue = testlist.PushFront("A");

		cout << "Expected value:" << expectedValue << endl;
		cout << "Actual values:" << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test Pass" << endl;
		}
		else
		{
			cout << "Test False" << endl;
		}
	}
}

void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;

    // Put tests here
	{
		cout << endl << "Test 1, creat a list and inset one item" << endl;

		List<string> testlist;
		bool expectedValue = true;
		bool actualValue = testlist.PushBack("A");

		cout << "Expected value:" << expectedValue << endl;
		cout << "Actual values:" << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test Pass" << endl;
		}
		else
		{
			cout << "Test False" << endl;
		}
	}

	{
		//test 2
		cout << "Test 2, insert 101 items" << endl;
		List<string> testlist;
		for (int i = 0; i < 100; i++)
			testlist.PushBack("Z");

		bool expectedValue = false;
		bool actualValue = testlist.PushBack("A");

		cout << "Expected value:" << expectedValue << endl;
		cout << "Actual values:" << actualValue << endl;

		if (expectedValue == actualValue)
		{
			cout << "Test Pass" << endl;
		}
		else
		{
			cout << "Test False" << endl;
		}
	}
}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;

    // test begin
	{
		cout << "Test 1, empty list" << endl;
		List<int> testlist;
		bool expectedOut = false;
		bool actualOut = testlist.PopFront();

		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;

		if (expectedOut == actualOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{ //test 2
		cout << "Test 2, list with one item." << endl;
		List<int> testlist;
		testlist.PushBack(1);
		bool expectedOut = true;
		bool actualOut = testlist.PopFront();
		
		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;

		if (expectedOut == actualOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{//test 3
		cout << "Test 3, Full list." << endl;
		List<int> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack(i);
		}
		bool expectedOut = false;
		bool actualOut = testlist.PopFront();

		

		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;

		if (expectedOut == actualOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;

    // Put tests here
	{
		cout << "Test 1, empty list." << endl;
		List<int> testlist;
		bool expectedOut = false;
		bool actualOut = testlist.PopBack();

		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;

		if (expectedOut == actualOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{ //test 2
		cout << "Test 2, List with one item." << endl;
		List<int> testlist;
		testlist.PushBack(1);
		bool expectedOut = true;
		bool actualOut = testlist.PopBack();
		
		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;

		if (expectedOut == actualOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{//test 3
		cout << "Test 3, Full list" << endl;
		List<int> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack(i);
		}
		bool expectedOut = false;
		bool actualOut = testlist.PopBack();

		

		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;

		if (expectedOut == actualOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}

void Tester::Test_Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;

    // Put tests here
	{

		cout << "Test 1, empty list." << endl;
		List<int> testlist;
		testlist.Clear();
		bool expectedOut = false;
		bool actualOut = testlist.IsEmpty();

		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;

		if (expectedOut == actualOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{ //test 2
		cout << "Test 2, one item in list." << endl;
		List<int> testlist;
		bool expectedOut = true;
		testlist.PushBack(1);
		testlist.Clear();
		bool actualOut = testlist.IsEmpty();
		
		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;

		if (expectedOut == actualOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{//test 3
		cout << "Test 3, full list." << endl;
		List<int> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack(i);
		}
		bool expectedOut = false;
		testlist.Clear();
		bool actualOut = testlist.IsEmpty();

		

		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;

		if (expectedOut == actualOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;

    // Put tests here
	
	{ // test 2
		cout << "Test 2, list with one item." << endl;
		List<string> testlist;
		testlist.PushBack("A");
		string expectedOut = "A";
		string * actualOut = testlist.Get(1);

		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;

		//if (expectedOut == *actualOut)
		//{
			//cout << "Pass" << endl;
		//}
		//else
		//{
			//cout << "Fail" << endl;
		//}
	}
	{ // test 3
		cout << "Test 3, full list." << endl;
		List<int> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack(i);
		}
		int expectedOut = 19;
		int * actualOut = testlist.Get(20);

		cout << "Expected Out:" << expectedOut << endl;
		cout << "Actual Out:" << actualOut << endl;
		/*
		if (expectedOut == *actualOut)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}*/
	}
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

    // Test 1
	{
		cout << "Test 1, empty list." << endl;
		List<int> testlist;
		int expectedout = NULL;
		int actualout = *testlist.GetFront();
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;
/*
		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}*/
	}
	{
		//test 2
		cout << "Test 2, List with one item." << endl;
		
		List<int> testlist;
		testlist.PushBack(1);
		int expectedout = 1;
		int actualout = *testlist.GetFront();
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;
/*
		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}*/

	}
	// test 3
	{
		cout << "Test 2, Full list." << endl;

		List<int> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack(i);
		}
		int expectedout = 0;
		int actualout = *testlist.GetFront();
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;
		/*
		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
		*/
	}
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

    // test 1
	{
		cout << "Test 1, empty list." << endl;

		List<int> testlist;
		
		int expectedout = NULL;
		int actualout = *testlist.GetBack();
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;
/*
		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}*/
	}
	{ //test 2
		cout << "Test 2, List with one item." << endl;

		List<int> testlist;
		testlist.PushBack(1);
		int expectedout = 1;
		int actualout = *testlist.GetBack();
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;
		/*
		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}*/
	}
	{ //test 3
		cout << "Test 3, Full list." << endl;

		List<int> testlist;
		for (int i = 0; i < 100; i++)
		{
			testlist.PushBack(i);
		}
		int expectedout = 99;
		int actualout = *testlist.GetBack();
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;
		/*
		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}*/
	}
}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;

    // test 1
	{
		
			cout << "Test 1, empty list." << endl;

			List<int> testlist;

			int expectedout = NULL;
			int actualout = testlist.GetCountOf(1);
			cout << "Expected Out:" << expectedout << endl;
			cout << "Actual Out:" << actualout << endl;

			if (expectedout == actualout)
			{
				cout << "Pass" << endl;
			}
			else
			{
				cout << "Fail" << endl;
			}
	}
	{ //test 2
		cout << "Test 2, List with one item." << endl;

		List<int> testlist;
		testlist.PushBack(1);
		int expectedout = 1;
		int actualout = testlist.GetCountOf(1);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	{ //test 3
		cout << "Test 3, Full list." << endl;

		List<int> testlist;
		for (int i = 0; i < 50; i++)
		{
			testlist.PushBack(i);
			testlist.PushBack(51);
		}
		int expectedout = 1;
		int actualout = testlist.GetCountOf(51);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}

void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

    // test 1
	{
		cout << "Test 1, empty list." << endl;
		List<int> testlist;
		bool expectedout = NULL;
		bool actualout = testlist.Contains(1);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{//test 2
		cout << "Test 2, list with one item." << endl;
		List<int> testlist;
		testlist.PushFront(1);
		bool expectedout = true;
		bool actualout = testlist.Contains(1);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{//test 3
		cout << "Test 2, Full list." << endl;
		List<int> testlist;
		for (int i = 0; i < 100; i++) {
			testlist.PushFront(i);
		}
		bool expectedout = true;
		bool actualout = testlist.Contains(50);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
}

void Tester::Test_Remove()
{
    DrawLine();
    cout << "TEST: Test_Remove" << endl;

    // test 1
	{
		const int x = 1;
		cout << "Test 1, empty list" << endl;
		List<string> testlist;
		bool expectedout = false;
		testlist.Remove(1);
		bool actualout = testlist.Remove(x);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	// test 2
	{
		cout << "Test 2, remove one item to the list." << endl;
		List<string> testlist;
		for (int i = 0; i < 5; i++) {
			testlist.PushBack("A");
		}
		bool expectedout = true;
		bool actualout = testlist.Remove(1);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	// test 3
	{
		cout << "Test 3, add one item to the list." << endl;
		List<string> testlist;
		for (int i = 0; i < 100; i++) {
			testlist.PushBack("A");
		}
		bool expectedout = true;
		bool actualout = testlist.Remove(3);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}
}

void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;

    // test 1
	{
		cout << "Test 1, empty list" << endl;
		List<int> testlist;
		bool expectedout = true;
		bool actualout = testlist.Insert(1,1);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	// test 2
	{
		cout << "Test 2, add one item to the list." << endl;
		List<int> testlist;
		for (int i = 0; i < 5; i++) {
			testlist.PushBack(i);
		}
		bool expectedout = true;
		bool actualout = testlist.Insert(3, 1);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	// test 3
	{
		cout << "Test 3, add one item to the full list." << endl;
		List<int> testlist;
		for (int i = 0; i < 100; i++) {
			testlist.PushBack(i);
		}
		bool expectedout = false;
		bool actualout = testlist.Insert(3, 1);
		cout << "Expected Out:" << expectedout << endl;
		cout << "Actual Out:" << actualout << endl;

		if (expectedout == actualout)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}
}
