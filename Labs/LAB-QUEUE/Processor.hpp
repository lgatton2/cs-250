#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "First Come First Served (FCFS)" << endl;
	output << "Job#0" << "   " << "Remaing jobs: " << jobQueue.Size();
	for (int i = 0; jobQueue.Size() > 0; i++) {
		jobQueue.Front()->Work(FCFS);
		if (jobQueue.Front()->fcfs_done)
		{
			int counter = 1;
			jobQueue.Front()->SetFinishTime(i, FCFS);
			jobQueue.Pop();
			output << "Job #" << counter << "   Done" << endl;
		}
		output << "Cycle:" << i << "    " <<"Remaing jobs: " << jobQueue.Size() << endl;
	}
	output.close();
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Round Robin (rr)" << endl;
	output << "Job#0" << "   " << "Remaing jobs: " << jobQueue.Size();
	jobQueue.Front()->rr_timesInterrupted = 1;
	for (int i = 0; jobQueue.Size() > 0; i++) {
		jobQueue.Front()->rr_timesInterrupted++;
		jobQueue.Front()->Work(RR);
		jobQueue.Push(jobQueue.Front());
		jobQueue.Pop();
		if (jobQueue.Front()->rr_done)
		{
			int counter = 1;
			jobQueue.Front()->SetFinishTime(i, RR);
			jobQueue.Pop();
			output << "Times interupted: " <<jobQueue.Front()->rr_timesInterrupted << "   Done" << endl;
		}
		output << "Cycle:" << i << "    " << "Remaing jobs: " << jobQueue.Size() <<"   time Remaining:" << jobQueue.Front()->rr_timeRemaining <<  endl;
	}
	output.close();
	
	output.close();
}

#endif
