#ifndef _function3
#define _function3

/*
Factorial functions
@param int n        The n! value

Calculate n! by multiplying n * (n-1) * (n-2) * ... * 3 * 2 * 1.
*/

int Factorial_Iter( int n )
{
	int temp = 1;
	for (int i = n; i > 1; i--)
	{
		temp = temp*i;
	}
    return temp; // placeholder;
}

int Factorial_Rec( int n )
{
	if (n == 1)
	{
		return n;
	}
	else
	{

		return (n*Factorial_Rec(n - 1)); // placeholder
	}
}

#endif
