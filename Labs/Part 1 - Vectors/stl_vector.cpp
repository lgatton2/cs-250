// Lab - Standard Template Library - Part 1 - Vectors
// FIRSTNAME, LASTNAME

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	
	vector<string> courses; //string[] courses
	
	bool done = false;
	while (!done)
	{
		cout << "Main Menu" << endl
			<< "1. Add a new course, "
			<< "2. Temove the lase course, "
			<< "3. Display the course list,  "
			<< "4. Quit" << endl;
		int choice;
		cin >> choice;

		if (choice == 1) { // add course
			string courseName; //temp variable
			cout << "please enter the course: ";
			cin.ignore();
			getline(cin, courseName);
			courses.push_back(courseName); // add to list of courses
		}
		else if (choice == 2){ // remove course
			courses.pop_back();// remove last item in the lis of courses
		}
		else if (choice == 3){
			for (unsigned int i = 0; i < courses.size(); i++) // make unsined because it can never be neg
			{
				cout << i << ". " << courses[i] << endl;
			}

		}
		else if (choice == 4)
		{
			done = true;
		}

	}

  
    return 0;
}
