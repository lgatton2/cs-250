#include <iostream>
using namespace std;

int Factorial(int n)
{
	//n!=N*(n-1)
	for (int i = n - 1; i > 0; i--)
	{
		n *= i;
	}
	return n;
}

int Factorial_Rec(int n)
{
	//termination case
	if (n == 0)
	{
		return 1;
	}

	//recursive case
	return (n * Factorial_Rec(n - 1));

}

void main()
{
	cout << Factorial(5);
	cout << endl << endl;

	cout << Factorial_Rec(5);

	system("pause");
}