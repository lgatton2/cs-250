#include <iostream>
using namespace std;

void CountUp(int start, int end)
{
	for (int i=start;i<=end;i++)
	{
		cout << i << " ";
	}


}

//1to5
void CountUp_Rec(int start, int end)
{
	cout << start << " ";
	// terminating case
	if (start == end)
	{
		return;
	}
	CountUp_Rec(start + 1, end);
}


void main() {
	CountUp(5, 15);

	cout << endl << endl;

	CountUp_Rec(5, 15);


	system("pause");
}